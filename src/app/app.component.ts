import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterOutlet } from '@angular/router';
import { PokemonService } from './shared/services/pokemon.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.sass',
})
export class AppComponent implements OnInit {
  pokemonList: { name: string; url: string }[] = [];
  isLoading: boolean = false;
  constructor(
    private pokemonService: PokemonService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe({
      next: (res) => {
        console.log(res);
      },
    });
    // consol
    // this.fetchPokemons();

    // this.pokemonService.listAll().subscribe({
    //   next: this.handleSuccess.bind(this),
    //   error: this.handleError.bind(this),
    // });
  }

  private fetchPokemons() {
    // Make sure to subscribe to the observable
    const resolvedData = this.activatedRoute.snapshot;
    console.log(resolvedData);
    // const res = this.activatedRoute.snapshot.data;
    // console.log(res);
    // this.isLoading = true;
    // this.pokemonService.listAll().subscribe({
    //   next: this.handleSuccess.bind(this),
    //   error: this.handleError.bind(this),
    // });
  }

  private handleSuccess(response: any) {
    this.isLoading = false;
    this.pokemonList = response.results;
  }

  private handleError(error: any) {
    this.isLoading = false;
    console.error(error);
  }
}
