import { Injectable, inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  ResolveFn,
  RouterStateSnapshot,
} from '@angular/router';
import { PokemonService } from '../services/pokemon.service';
import { catchError, of } from 'rxjs';

// export const pokemonResolver: ResolveFn<any> = (
//   route: ActivatedRouteSnapshot,
//   state: RouterStateSnapshot
// ) => {
//   let pokemonResults: any[] = [];

//   console.log(state.url);
//   return inject(PokemonService).listAll();
// };
@Injectable({
  providedIn: 'root',
})
export class PokemonResolver implements Resolve<any> {
  constructor(private readonly pokemonService: PokemonService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.pokemonService.listAll();
  }
}
