import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { enviroment } from '../../../enviroment/api-enviroment';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private http: HttpClient) {}

  listAll(): Observable<any> {
    return this.http.get('https://pokeapi.co/api/v2/pokemon/');
  }
}
