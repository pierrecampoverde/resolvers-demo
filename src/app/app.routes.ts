import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PokemonResolver } from './shared/resolvers/pokemon.resolver';

export const routes: Routes = [
  { path: '', redirectTo: 'pokemons', pathMatch: 'full' },
  {
    path: 'pokemons',
    component: AppComponent,
    resolve: {
      pokemons: PokemonResolver,
    },
  },
];
